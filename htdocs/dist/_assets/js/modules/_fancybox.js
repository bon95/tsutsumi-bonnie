export const fancybox = () => {
  $('[data-fancybox]').fancybox({
    toolbar  : false,
    smallBtn : true,
    iframe : {
      preload : false
    },
    'titleFormat'    : formatTitle,
  'titlePosition': 'inside'
  })
}