export const burgerToggle = () => {

  $("#burger-btn").click(() => {
    $(".siteHeader__nav").toggleClass("show");
    $("#burger-btn").toggleClass("open");
  })
  
}